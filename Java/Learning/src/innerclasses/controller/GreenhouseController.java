package innerclasses.controller;

import innerclasses.controller.*;

class Optioned extends GreenhouseControls {

    public class OptionSwitcher extends Event {
        private int counter = 0;

        OptionSwitcher(long delayTime) {

            super(delayTime);

        }

        public void switcher() {
            if (counter % 2 == 0)
                optionStatus = false;
            else optionStatus = true;
            counter++;
        }

        @Override
        public void action() {

        }

        @Override
        public String toString() {

            if (optionStatus == false)
            return "Option OFF";
            else return "Option ON";
        }
    }
}

public class GreenhouseController {
    public static void main(String[] args) {
        args = new String[1];
        args[0] = "5000";
        Optioned gc = new Optioned();
        Optioned.OptionSwitcher ss = gc.new OptionSwitcher(900);
        gc.addEvent(gc.new Bell(900));
        ss.switcher();
        ss.switcher();
        Event[] eventList = {
                gc.new ThermostatNight(0),
                gc.new LightOn(200),
                gc.new LightOff(400),
                gc.new WaterOn(600),
                gc.new WaterOff(800),
                gc.new AirCycleOn(500),
                gc.new AirCycleOff(300),
                gc.new ThermostatDay(1400)
        };
        gc.addEvent(gc.new Restart(2000, eventList));
        if (args.length == 1)
            gc.addEvent(new GreenhouseControls.Terminate(new Integer(args[0])));

        gc.run();
    }
}
