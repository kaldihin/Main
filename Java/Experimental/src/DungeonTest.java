import java.io.*;

class DungeonTest {

    class DungeonGame implements Serializable {

        public int x = 3;
        transient long y = 4;
        private short z = 5;

        int getX() {
            return x;
        }

        long getY() {
            return y;
        }

        short getZ() {
            return z;
        }

    }

    public static void main(String[] args) {
        DungeonGame d = new DungeonGame();

        try {
            FileInputStream fis = new FileInputStream("dg.ser");
            ObjectInputStream ois = new ObjectInputStream(fis);

            d = (DungeonGame) ois.readObject();

            System.out.println(d.getX() + d.getY() + d.getZ());



            FileOutputStream fos = new FileOutputStream("dg.ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            System.out.println(d.getX() + d.getY() + d.getZ());

            oos.writeObject(d);

            ois.close();
            oos.close();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
