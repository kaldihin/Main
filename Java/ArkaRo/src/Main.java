import game.BaseGame;

public class Main {
    public static void main(String[] args) {
        BaseGame baseGame = new BaseGame();
        baseGame.windowCreate();
        baseGame.gameInitialize();
    }
}
