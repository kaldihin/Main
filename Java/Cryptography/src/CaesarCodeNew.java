import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.io.*;

public class CaesarCodeNew {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        String A = new String();
        String B = new String();
        int m = 0,c,cInt,n= 4,k = 5;
        String nC, kC;

        //чтение ключа

        try(BufferedReader br = new BufferedReader(new FileReader("E:\\Cryptography\\src\\Key.txt")))
        {
            //чтение построчно
            String s;
            int i = 0;
            while((s=br.readLine())!=null){

                System.out.println(s);

                if(i == 0) {
                    nC = s.substring(2);
                    n = Integer.parseInt(nC);
                    i++;
                }

                kC = s.substring(2);
                k = Integer.parseInt(kC);

            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }





        //чтение алфавита

        try(BufferedReader br = new BufferedReader(new FileReader("E:\\Cryptography\\src\\Alpha.txt")))
        {
            //чтение построчно
            String s;
            while((s=br.readLine())!=null){

                System.out.println(s);
                if(s.charAt(0) == '!')
                    System.out.println("Its number is:" + s.substring(2));
            }
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        //Подсчет алфавита

        try(FileReader reader = new FileReader("E:\\Cryptography\\src\\Code.txt"))
        {

            while((c = reader.read()) != -1){

                A += (char)c;
                m++;

            }

        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        try(FileWriter writer = new FileWriter("E:\\Cryptography\\src\\Code.txt", false))
        {
            // запись всей строки
            writer.write(A);
            //String text = "Мама мыла раму, раму мыла мама";
            //writer.write(text);
            // запись по символам
            //writer.append('\n');
            //writer.append('E');

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        //Шифровка

        try(FileReader reader = new FileReader("E:\\Cryptography\\src\\Code.txt"))
        {

            while((c = reader.read()) != -1){

                cInt = A.charAt(3);
                cInt = (c*n + k) % m;
                B += (char) cInt;

            }

        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        try(FileWriter writer = new FileWriter("E:\\Cryptography\\src\\CodeS.txt", false))
        {
            writer.write(B);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        //Дешифровка

        int power = -1;
        double nD = (double) n;

        try(FileReader reader = new FileReader("E:\\Cryptography\\src\\CodeS.txt"))
        {

            while((c = reader.read()) != -1){

                cInt = (c - k) * (int)Math.pow(n, power) % m;
                A += (char) cInt;

            }

        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        try(FileWriter writer = new FileWriter("E:\\Cryptography\\src\\CodeD.txt", false))
        {
            writer.write(A);

            writer.flush();
        }
        catch(IOException ex){

            System.out.println(ex.getMessage());
        }

        System.out.println("Here they are: " + n + " " + k);

        System.out.println(Math.max(0.00002 , 0.00003));

    }
}
